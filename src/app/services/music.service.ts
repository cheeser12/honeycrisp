import { Injectable } from '@angular/core';

import { Song } from '../models/Song';

declare var MusicKit: any;

@Injectable({
  providedIn: 'root'
})
export class MusicService {
  private static limit = 100;
  private static chunkSize = 1000;

  private music: any;

  constructor() {
    MusicKit.configure({
      developerToken: 'eyJhbGciOiJFUzI1NiIsInR5cCI6IkpXVCIsImtpZCI6IkszUlo2MjNNOTcifQ.eyJpYXQiOjE1NDEzODk2NjksImV4cCI6MTU1Njk0MTY2OSwiaXNzIjoiU0M3SjJKNzhSNiJ9.0zfikOWAgxRc8IZkwnbwkfUd4UNEkohpfIy-G7GX4jUdtcBJFMaD54F407VRQk_uf_RxQ_NP8i1rgvgfpSzlBg',
      app: {
        name: 'Honeycrisp',
        build: '0.0.1'
      }
    });

    this.music = MusicKit.getInstance();
  }

  authorize() {
    this.music.unauthorize();
    this.music.authorize();
  }

  isAuthorized(): boolean {
    return this.music.isAuthorized;
  }

  playSong(id: string) {
    this.music.setQueue({song: id}).then(() => {
      this.music.play();
    });
  }

  pause() {
    this.music.pause();
  }

  play() {
    this.music.play();
  }

  loadSongs(): Promise<Song[]> {
    return this.loadChunkOfSongs(1);
  }

  private loadChunkOfSongs(chunkIndex: number): Promise<Song[]> {
    let offset = (MusicService.chunkSize * (chunkIndex - 1));
    const promises: Promise<Song[]>[] = [];

    return new Promise((res) => {
      while (offset + MusicService.limit <= MusicService.chunkSize * chunkIndex) {
        promises.push(this.getSongs(offset));

        offset += MusicService.limit;
      }

      Promise.all(promises).then((compoundSongs) => {
        let songs = [].concat.apply([], compoundSongs);
        if (songs.length === MusicService.chunkSize) {
          this.loadChunkOfSongs(chunkIndex + 1).then((moreSongs) => {
            songs = songs.concat(moreSongs);
            res(songs);
          });
        } else {
          res(songs);
        }
      });
    });
  }

  private getSongs(offset: number): Promise<Song[]> {
    return new Promise((res) => {
      this.music.api.library.songs(null, {limit: MusicService.limit, offset: offset}).then((data) => {
        const songs = data.map((song) => {
          return new Song(song.id, song.attributes.name,
            song.attributes.artistName,
            song.attributes.albumName,
            song.attributes.trackNumber,
            song.attributes.artwork.url);
        });

        res(songs);
      });
    });
  }
}
