import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { NowPlayingModule } from './now-playing/now-playing.module';
import { MusicLibraryModule } from './music-library/music-library.module';

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    NowPlayingModule,
    MusicLibraryModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
