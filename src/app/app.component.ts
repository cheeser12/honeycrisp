import { Component } from '@angular/core';

import { MusicService } from './services/music.service';
import { Song } from './models/Song';
import { LibraryArtist } from './models/LibraryArtist';
import { LibraryAlbum } from './models/LibraryAlbum';

declare var MusicKit: any;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  songs: Song[];
  nowPlaying: Song;
  library: LibraryArtist[];
  constructor(private musicService: MusicService) {
  }

  title = 'honeycrisp';

  ngOnInit() {
    if (!this.musicService.isAuthorized()) {
      this.musicService.authorize();
    }
  }

  loadSongs() {
    this.musicService.loadSongs().then((songs) => {
      this.library = songs.reduce<LibraryArtist[]>((library, song) => {
        const existingArtist = library.find((artist: LibraryArtist) => {
          return artist.name === song.artist;
        });

        if (existingArtist) {
          const existingAlbum = existingArtist.albums.find((album: LibraryAlbum) => {
            return album.name === song.album;
          });

          if (existingAlbum) {
            existingAlbum.songs.push(song);

            existingAlbum.songs = this.sortSongs(existingAlbum.songs);
          } else {
            const newAlbum = new LibraryAlbum(song.album);
            newAlbum.songs.push(song);

            existingArtist.albums.push(newAlbum);
          }
        } else {
          const newAlbum = new LibraryAlbum(song.album);
          newAlbum.songs.push(song);

          const newArtist = new LibraryArtist(song.artist);
          newArtist.albums.push(newAlbum);

          library.push(newArtist);
        }

        return library;
      }, []);

      this.library = this.library.sort((a, b) => {
        const artist1 = a.name.toUpperCase();
        const artist2 = b.name.toUpperCase();

        if (artist1 > artist2) {
          return 1;
        } else if (artist1 < artist2) {
          return -1;
        } else {
          return 0;
        }
      });
    });
  }

  sortSongs(songs: Song[]): Song[] {
    return songs.sort((a, b) => {
      if (a.trackNumber > b.trackNumber) {
        return 1;
      } else if (a.trackNumber < b.trackNumber) {
        return -1;
      } else {
        return 0;
      }
    });
  }

  playSong(song) {
    this.musicService.playSong(song.id);
    this.nowPlaying = song;
  }

  pause() {
    this.musicService.pause();
  }

  play() {
    this.musicService.play();
  }
}
