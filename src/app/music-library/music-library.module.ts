import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MusicLibraryComponent } from './containers/music-library.component';

@NgModule({
  imports: [CommonModule],
  declarations: [MusicLibraryComponent],
  exports: [MusicLibraryComponent]
})
export class MusicLibraryModule {}
