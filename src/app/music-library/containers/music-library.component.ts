import { Component, Input, Output, EventEmitter } from '@angular/core';
import { LibraryAlbum } from 'src/app/models/LibraryAlbum';
import { LibraryArtist } from 'src/app/models/LibraryArtist';
import { Song } from 'src/app/models/Song';

@Component({
  selector: 'app-music-library',
  styleUrls: ['music-library.component.scss'],
  template:
  `
    <div class="libraryContainer">
    <ul class="libraryPane">
      <li *ngFor="let artist of this.artists" (click)="selectArtist(artist)">
        {{artist.name}}
      </li>
    </ul>
    <ul class="libraryPane">
      <li *ngFor="let album of this.artist?.albums" (click)="selectAlbum(album)">
        {{album.name}}
      </li>
    </ul>
    <ul class="libraryPane">
      <li *ngFor="let song of this.album?.songs" (click)="selectSong(song)">
        {{song.trackNumber}}. {{song.name}}
      </li>
    </ul>
    </div>
  `
})
export class MusicLibraryComponent {
  @Input() artists: LibraryArtist[];
  @Output() songSelected = new EventEmitter();

  artist: LibraryArtist;
  album: LibraryAlbum;

  selectArtist(artist: LibraryArtist) {
    this.artist = artist;
    this.album = null;
  }

  selectAlbum(album: LibraryAlbum) {
    this.album = album;
  }

  selectSong(song: Song) {
    this.songSelected.emit(song);
  }
}
