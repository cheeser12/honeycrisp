import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Song } from 'src/app/models/Song';

@Component({
  selector: 'app-now-playing',
  styleUrls: ['now-playing.component.scss'],
  template:
  `
    <div class="now-playing-wrapper">
    <footer class="now-playing">
      <div class="album-art">
        <img [src]="song.getFormattedArtwork(100, 100)">
      </div>
      <div class="song-control">
        <div class="song-info-header">
          <i class="fa fa-chevron-down"></i>
        </div>
        <div class="song-info">
          {{song?.name}}
          <br/>
          {{song?.artist}}
          <br/>
          {{song?.album}}
        </div>
        <div class="song-control-buttons">
          <i class="fa fa-step-backward"></i>
          <i class="fa fa-pause" (click)="pauseSong();"></i>
          <i class="fa fa-play" (click)="playSong();"></i>
          <i class="fa fa-step-forward"></i>
        </div>
      </div>
      <div class="song-queue-button">
        <i class="fa fa-music"></i>
      </div>
    </footer>
    </div>
  `
})
export class NowPlayingComponent {
  @Input() song: Song;
  @Output() pause = new EventEmitter();
  @Output() play = new EventEmitter();

  pauseSong() {
    this.pause.emit(null);
  }

  playSong() {
    this.play.emit(null);
  }
}
