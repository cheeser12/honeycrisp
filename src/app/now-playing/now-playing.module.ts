import { NgModule } from '@angular/core';

import { NowPlayingComponent } from './containers/now-playing.component';

@NgModule({
  imports: [],
  declarations: [NowPlayingComponent],
  exports: [NowPlayingComponent]
})
export class NowPlayingModule {}
