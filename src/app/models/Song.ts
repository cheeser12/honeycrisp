export class Song {
  constructor(public id: string, public name: string, public artist: string, public album: string, public trackNumber: number, public artworkUrl: string) {

  }

  public getFormattedArtwork(width: number, height: number) {
    return this.artworkUrl.replace('{w}', width.toString()).replace('{h}', height.toString());
  }
}
