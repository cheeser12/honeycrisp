import { Song } from './Song';

export class LibraryAlbum {
  public songs: Song[] = [];

  constructor(public name: string) {

  }
}
