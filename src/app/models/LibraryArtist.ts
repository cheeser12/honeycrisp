import { LibraryAlbum } from './LibraryAlbum';

export class LibraryArtist {
  constructor(public name: string) {

  }

  public albums: LibraryAlbum[] = [];
}
