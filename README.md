# **This project is no longer active**
I no longer use Apple Music, so I've rebranded Honeycrisp as a Spotify utility
See here: [Honeycrisp](https://gitlab.com/cheeser12/honeycrisp-frontend)

# Honeycrisp

Honeycrisp is an alternative music player for Apple Music. I'm building it as a way to learn Angular, so it will certainly need tweaking over time.

## Instructions

Download the project, install the dependencies, and run `ng serve`

We need more documentation here!

## Goals

- Make a fully functioning web music player that's a good lightweight alternative to ITunes for Apple Music
- Make it fast
- Last.fm integration
- Customizable (colors, maybe layout?)
- Mobile friendly

## License

GPLv3. See [LICENSE](LICENSE)

## FAQ

### Q: Where are the emojis? All the cool projects have them.
A: You get ONE! 🐯
